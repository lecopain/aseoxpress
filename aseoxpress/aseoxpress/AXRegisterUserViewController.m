//
//  AXRegisterUserViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/26/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXRegisterUserViewController.h"
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import "AXLoginUIViewController.h"
#import <QuartzCore/QuartzCore.h>
#define kOFFSET_FOR_KEYBOARD 100.0



@interface AXRegisterUserViewController ()

@end

@implementation AXRegisterUserViewController

@synthesize municipio = _municipio;
@synthesize formaPago = _formaPago;
@synthesize medio = _medio;
@synthesize banios = _banios;
@synthesize recamaras = _recamaras;
@synthesize cityPickerView = _cityPickerView;
@synthesize paymentpickerView = _paymentpickerView;
@synthesize roomPickerView = _roomPickerView;
@synthesize bathPickerView = _bathPickerView;
@synthesize mediumPickerView = _mediumPickerView;

//fields
@synthesize nombreCompleto = _nombreCompleto;
@synthesize celular = _celular;
@synthesize domicilio = _domicilio;
@synthesize colonia = _colonia;
@synthesize referencias = _referencias;
@synthesize email = _email;
@synthesize contrasena = _contrasena;
@synthesize confirmarContrasena = _confirmarContrasena;

@synthesize costLabel = _costLabel;
@synthesize durationLabel = _durationLabel;

//UITextFields
@synthesize citySelectorUITextField = _citySelectorUITextField;
@synthesize paymentSelectorUITextField = _paymentSelectorUITextField;
@synthesize roomSelectorUITextField = _roomSelectorUITextField;
@synthesize bathroomSelectorUITextField = _bathroomSelectorUITextField;
@synthesize contactMediumUITextField = _contactMediumUITextField;

@synthesize scrollView = _scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _municipio = @[@"Tlajomulco", @"Tlaquepaque", @"Guadalajara", @"Zapopan", @"Tonala", @"El Salto"];
    _formaPago = @[@"OXXO", @"Banamex", @"Transferencia", @"Tarjeta"];
    _recamaras = @[@"1", @"2", @"3", @"4", @"5", @"6"];
    _banios = @[@"1", @"1.5", @"2", @"2.5", @"3", @"3.5", @"4", @"4.5", @"5", @"5.5", @"6"];
    _medio = @[@"Facebook", @"Google", @"Un amigo", @"Revista", @"Otro"];
    
    //_cityPickerView.hidden = YES;
    [_cityPickerView selectRow:2 inComponent:0 animated:NO];
    [_citySelectorUITextField setInputView:_cityPickerView];
    _citySelectorUITextField.borderStyle=UITextBorderStyleNone;
    
    [_paymentSelectorUITextField setInputView:_paymentpickerView];
    _paymentSelectorUITextField.borderStyle=UITextBorderStyleNone;
    
    [_roomSelectorUITextField setInputView:_roomPickerView];
    _roomSelectorUITextField.borderStyle=UITextBorderStyleNone;

    [_bathroomSelectorUITextField setInputView:_bathPickerView];
    _bathroomSelectorUITextField.borderStyle=UITextBorderStyleNone;
    
    [_contactMediumUITextField setInputView:_mediumPickerView];
    _contactMediumUITextField.borderStyle=UITextBorderStyleNone;
    

 
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    

}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void) cityPicker:(UIGestureRecognizer *)sender{

    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == _cityPickerView){
        return _municipio.count;
    }
    if(pickerView == _paymentpickerView){
        return _formaPago.count;
    }
    if(pickerView == _roomPickerView){
        return _recamaras.count;
    }
    if(pickerView == _bathPickerView){
        return _banios.count;
    }
    if(pickerView == _mediumPickerView){
        return _medio.count;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if(pickerView == _cityPickerView){
        return _municipio[row];
    }
    if(pickerView == _paymentpickerView){
        return _formaPago[row];
    }
    if(pickerView == _roomPickerView){
        return _recamaras[row];
    }
    if(pickerView == _bathPickerView){
        return _banios[row];
    }
    if(pickerView == _mediumPickerView){
        return _medio[row];
    }
    return @"";
}



- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] initWithFrame:CGRectMake(pickerView.frame.size.width/2, 0, pickerView.frame.size.width, pickerView.frame.size.height)];
        [tView setFont:[UIFont fontWithName:@"Helvetica" size:20]];

        [tView setBounds:CGRectMake(pickerView.frame.size.width/2, 0, pickerView.frame.size.width, pickerView.frame.size.height)];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=5;
        
        
    }
    // Fill the label text here
    
    
    if(pickerView == _cityPickerView){
        tView.text=[_municipio objectAtIndex:row];

    }
    if(pickerView == _paymentpickerView){
        tView.text=[_formaPago objectAtIndex:row];
    }
    if(pickerView == _roomPickerView){
        tView.text=[_recamaras objectAtIndex:row];
    }
    if(pickerView == _bathPickerView){
        tView.text=[_banios objectAtIndex:row];
    }
    if(pickerView == _mediumPickerView){
        tView.text=[_medio objectAtIndex:row];
    }

    return tView;
}


#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    
    if(pickerView == _cityPickerView){
        _citySelectorUITextField.text = [_municipio objectAtIndex:row];
        [_citySelectorUITextField resignFirstResponder];
    }
    if(pickerView == _paymentpickerView){
        _paymentSelectorUITextField.text = [_formaPago objectAtIndex:row];
        [_paymentSelectorUITextField resignFirstResponder];

    }
    if(pickerView == _roomPickerView){
        _roomSelectorUITextField.text = [_recamaras objectAtIndex:row];
        NSDictionary * response = [self calcCostos:[_bathroomSelectorUITextField.text floatValue] recamaras:[_roomSelectorUITextField.text intValue]];
        _costLabel.text = [response objectForKey:@"cotizacion"];
        _durationLabel.text = [response objectForKey:@"duracion"];
        [_roomSelectorUITextField resignFirstResponder];
        
    }
    if(pickerView == _bathPickerView){
        _bathroomSelectorUITextField.text = [_banios objectAtIndex:row];
        NSDictionary * response = [self calcCostos:[_bathroomSelectorUITextField.text floatValue] recamaras:[_roomSelectorUITextField.text intValue]];
        _costLabel.text = [response objectForKey:@"cotizacion"];
        _durationLabel.text = [response objectForKey:@"duracion"];
        [_bathroomSelectorUITextField resignFirstResponder];

    }
    if(pickerView == _mediumPickerView){
        _contactMediumUITextField.text = [_medio objectAtIndex:row];
        [_contactMediumUITextField resignFirstResponder];
    }
    
       
}
- (BOOL)checkField:(UITextField *)field{
    if(![field.text isEqual: @""]){
        return YES;
    }
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == _confirmarContrasena){
        if(![self validatePasswords]){
            _confirmarContrasena.text = @"";
        }
    }
}

-(BOOL)validatePasswords{
    
    if(![_contrasena.text isEqual:_confirmarContrasena.text]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                 message:@"Las contraseñas no coinciden"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                             return;
                                                         }]; //You can use a block here to handle a press on this button
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}

-(IBAction)registrarUsuario:(id)sender{
    //[self.navigationController dismissViewControllerAnimated:NO completion:nil];
    //[[SlideNavigationController sharedInstance] dismissViewControllerAnimated:NO completion:nil];
    
    if(![self validatePasswords]){
        return;
    }
    
    if( [self checkField:_nombreCompleto] && [self checkField:_celular] && [self checkField:_domicilio]
       && [self checkField:_colonia] && [self checkField:_referencias] && [self checkField:_email]
       && [self checkField:_contrasena] && [self checkField:_confirmarContrasena]){
        
        NSString *post = [NSString stringWithFormat:@"MM_insert=formalta&nombre=%@&celular=%@&domicilio=%@&colonia=%@&referencia=%@&correo=%@&password=%@&municipio=%@&norecamaras=%@&nobanos=%@&formadepago=%@&entero=%@",
                          _nombreCompleto.text, _celular.text, _domicilio.text, _colonia.text, _referencias.text, _email.text, _contrasena.text, _citySelectorUITextField.text, _roomSelectorUITextField.text, _bathroomSelectorUITextField.text, _paymentSelectorUITextField.text, _contactMediumUITextField.text];
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/registroUsuario"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSLog(@"%@", response);
                                              NSLog(@"%@", error);
                                              
                                              if(data){
                                                  
                                                  NSError *e = nil;
                                                  NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                                  
                                                  if([[responseDict objectForKey:@"response"]  isEqual: @"error_user_exists"]){
                                                      
                                                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                               message:@"Error: esta cuenta ya existe"
                                                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                                      //We add buttons to the alert controller by creating UIAlertActions:
                                                      UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                         style:UIAlertActionStyleDefault
                                                                                                       handler:nil]; //You can use a block here to handle a press on this button
                                                      [alertController addAction:actionOk];
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self presentViewController:alertController animated:YES completion:nil];
                                                      });
                                                      
                                                  
                                                  }
                                                  
                                                  
                                                  if([[responseDict objectForKey:@"response"]  isEqual: @"success"]){

                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      
                                                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                               message:@"Gracias! Tu información ha sido enviada correctamente. A tu email acabamos de mandar el correo de bienvenida"
                                                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                                    
                                                    //We add buttons to the alert controller by creating UIAlertActions:
                                                          UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                             style:UIAlertActionStyleDefault
                                                                                                           handler:^(UIAlertAction * action){
                                                                                                               [self NavigateToAccount];
                                                                                                           }]; //You can use a block here to handle a press on this button
                                                          [alertController addAction:actionOk];
                                                          [self presentViewController:alertController animated:YES completion:nil];
                                                      });
                                                }
                                              }
                                          }];
        [dataTask resume];
    }
    else{
        NSLog(@"Campos incompletos");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                 message:@"Error: Campos incompletos"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil]; //You can use a block here to handle a press on this button
        [alertController addAction:actionOk];
        
        [self presentViewController:alertController animated:YES completion:nil];
        

    }
}

- (void) NavigateToAccount{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *staffViewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[storyboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    [self.navigationController presentViewController:slideNavigationController animated:NO completion:nil];
    
}



- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [_scrollView setScrollEnabled:YES];
    
    NSLog(@"%f", [[UIScreen mainScreen] bounds].size.height);
    if ([[UIScreen mainScreen] bounds].size.height == 568){ //iphone 5/5c/5s
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height + 580)];
    }
    else if ([[UIScreen mainScreen] bounds].size.height >= 667) //iphone 6/6s/6 plus
    {
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height + 480)];
    }
    else{ //iphone 3x/4/4s
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height +660)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSDictionary *) calcCostos:(float)NumeroBanos recamaras:(int)NumeroRecamaras{
    
    NSMutableDictionary * result = [[NSMutableDictionary alloc] init];
    int norecamaras= NumeroRecamaras;
    float nobanos= NumeroBanos;
    NSString * cotizacion = @"";
    NSString * duracion = @"";
    if(norecamaras==1 && (nobanos==1 || nobanos==1.5 || nobanos==2) ){
        cotizacion = @"$265";
        duracion = @"3 Horas";
    }
    if(norecamaras==1 && (nobanos==2.5 ) ){
        cotizacion=@"$315";
        duracion=@"4 Horas";
    }
    if(norecamaras==1 && (nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6) ){
        cotizacion=@"2 Personas";
        duracion=@"3 o 4 Horas";
    }
    /////////////
    if(norecamaras==2 && (nobanos==1 || nobanos==1.5 || nobanos==2 ) ){
        cotizacion=@"$265";
        duracion=@"3 Horas";
    }
    if(norecamaras==2 && (nobanos==2.5 ) ){
        
        cotizacion=@"$315";
        duracion=@"4 Horas";
    }
    if(norecamaras==2 && (nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6) ){
        cotizacion=@"2 Personas";
        duracion=@"3 o 4 Horas";
    }
    ///////
    if(norecamaras==3 && (nobanos==1 || nobanos==1.5 ) ){
        cotizacion=@"$315";
        duracion=@"4 Horas";
    }
    if(norecamaras==3 && (nobanos==2 || nobanos==2.5 ) ){
        cotizacion=@"$315";
        duracion=@"4 Horas";
    }
    if(norecamaras==3 && (nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6) ){
        cotizacion=@"2 Personas";
        duracion=@"3 o 4 Horas";
    }
    ////////////////////
    
    if(norecamaras==4 && (nobanos==1 || nobanos==1.5 || nobanos==2 || nobanos==2.5 || nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6) ){
        cotizacion=@"2 Personas";
        duracion=@"3 o 4 Horas";
    }
    /////////
				if(norecamaras==5 && (nobanos==1 || nobanos==1.5 || nobanos==2 || nobanos==2.5 || nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6) ){
                    cotizacion=@"2 Personas";
                    duracion=@"3 o 4 Horas";
                }
    ///////////////////
				if(norecamaras==6 && (nobanos==1 || nobanos==1.5 || nobanos==2 || nobanos==2.5 || nobanos==3 || nobanos==3.5 || nobanos==4 || nobanos==4.5 || nobanos==5 || nobanos==6)  ){
                    cotizacion=@"2 Personas";
                    duracion=@"3 o 4 Horas";
                }
    [result setValue:cotizacion forKey:@"cotizacion"];
    [result setValue:duracion forKey:@"duracion"];
    
    return result;
}


@end


