//
//  ViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//


#import "AXLoginUIViewController.h"
#import "AppDelegate.h"
#import "LeftMenuViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SlideNavigationController.h"
#import "AXRegisterUserViewController.h"
#import "AXUserForgotPasswordViewController.h"

#define kOFFSET_FOR_KEYBOARD 100.0


@interface AXLoginUIViewController ()

@end

@implementation AXLoginUIViewController

@synthesize userUITextField = _userUITextField;
@synthesize passwordUITextField = _passwordUITextField;
@synthesize scrollView = _scrollView;
@synthesize imageView = _imageView;

const int VIEW_OFFSET = 0;



- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_scrollView setScrollEnabled:YES];
    [_scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = _imageView.bounds;
    [_imageView addSubview:visualEffectView];

    
//    [_userUITextField setText:@"contacto@aseoxpress.com"];
//    [_userUITextField setText:@"israel.jauregui@egea.com.mx"];
//    [_passwordUITextField setText:@"123456"];
    
    [_userUITextField setDelegate:self];
    [_passwordUITextField setDelegate:self];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                  initWithTarget:self
                                  action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
//    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.center = self.view.center;
//    [self.view addSubview:loginButton];
//    
//
    
    FBSDKLikeControl *likeButton = [[FBSDKLikeControl alloc]
                                initWithFrame:CGRectMake(150, 150, 100, 50)];
    likeButton.objectID = @"https://www.facebook.com/aseoxpress";
    // Center Button
    CGRect bounds = self.view.bounds;
    likeButton.center = CGPointMake(
                                CGRectGetMidX(bounds), 100
                                );
    likeButton.foregroundColor = [UIColor blackColor];
    likeButton.likeControlStyle = FBSDKLikeControlStyleBoxCount;
    likeButton.likeControlHorizontalAlignment = FBSDKLikeControlHorizontalAlignmentCenter;
    
    [self.view addSubview:likeButton];
}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height + VIEW_OFFSET)];

    
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerUser:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    AXRegisterUserViewController *registerViewController = (AXRegisterUserViewController *)[storyboard instantiateViewControllerWithIdentifier:@"registerScreen"];
    
//    [self.navigationController presentViewController:registerViewController animated:NO completion:nil];
    
    [[SlideNavigationController sharedInstance] pushViewController:registerViewController animated:NO];
    
}

- (IBAction)userForgotPassword:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    AXUserForgotPasswordViewController *userForgotViewController = (AXUserForgotPasswordViewController *)[storyboard instantiateViewControllerWithIdentifier:@"userForgotPasswordScreen"];
    
    //    [self.navigationController presentViewController:registerViewController animated:NO completion:nil];
    
    [[SlideNavigationController sharedInstance] pushViewController:userForgotViewController animated:NO];
    
}


- (void) setupConnection{

    NSString *post = [NSString stringWithFormat:@"usuario=%@&password=%@",_userUITextField.text, _passwordUITextField.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/loginUsuario"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSLog(@"%@", response);
                                          NSLog(@"%@", error);
                                          if(data){
                                              
                                              NSError *e = nil;
                                              NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                              
                                              
                                              if([[responseDict objectForKey:@"response"]  isEqual: @"success"]){
                                                  
                                                  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                  //save current userId
                                                  NSString * userId = [responseDict objectForKey:@"id"];
                                                  [defaults setObject:userId forKey:@"userId"];

                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      

                                                  
                                                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                                                  
                                                  UIViewController *staffViewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"staffScreen"];
                                                  
                                                  LeftMenuViewController *leftMenu = (LeftMenuViewController*)[storyboard
                                                                                                               instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
                                                  
                                                  SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
                                                  [SlideNavigationController sharedInstance].leftMenu = leftMenu;
                                                  [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
                                                  [SlideNavigationController sharedInstance].enableShadow = YES;
                                                  [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
                                                  [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
                                                  
                                                  [self.navigationController presentViewController:slideNavigationController animated:NO completion:nil];
                                                      
                                                      
                                                  });
                                              }
                                              else{
                                                  
                                                  
                                                  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                           message:[responseDict objectForKey:@"response"]
                                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                                  //We add buttons to the alert controller by creating UIAlertActions:
                                                  UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                     style:UIAlertActionStyleDefault
                                                                                                   handler:nil]; //You can use a block here to handle a press on this button
                                                  [alertController addAction:actionOk];
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self presentViewController:alertController animated:YES completion:nil];
                                                  });
                                                  

                                              }
                                              
                                        }
                              
                                          
                                      }];
    [dataTask resume];
    }


- (IBAction)login:(id)sender{
    NSLog(@"%@", self.navigationController);
    NSLog(@"vcs:%@", self.navigationController.viewControllers);
    NSLog(@"parent:%@", self.navigationController.parentViewController);
    NSLog(@"keyWindow:%@",[UIApplication sharedApplication].keyWindow.rootViewController);
    //[self.navigationController dismissViewControllerAnimated:NO completion:nil];
    
//
//    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
//    
//    appDelegateTemp.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
    
    [self setupConnection];
    
}


-(void)keyboardWillHide {
    NSLog(@"%f", _scrollView.bounds.origin.y);
    [self setViewMovedUp:NO];
}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    //UITextField * textField = textField;
    NSLog(@"Frame: %f", [sender frame].origin.y);
    //
    if ([sender frame].origin.y > 0)
    {
        //move the main view, so that the keyboard does not hide it.
        //        if  (self.view.frame.origin.y >= 0)
        //        {
        [self setViewMovedUp:YES];
        //        }
    }
    else{
        [self setViewMovedUp:NO];
    }
}

-(void)dismissKeyboard {
    [_userUITextField resignFirstResponder];
    [_passwordUITextField resignFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)sender{

       [self setViewMovedUp:NO];

}
//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _scrollView.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        //rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        
        [_scrollView setContentInset:UIEdgeInsetsMake(-kOFFSET_FOR_KEYBOARD, 0, 0, 0)];
        [_scrollView layoutIfNeeded];
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        [_scrollView setContentInset:UIEdgeInsetsMake(60, 0, 0, 0)];
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height + VIEW_OFFSET)];

   
        [_scrollView layoutIfNeeded];

    }
    _scrollView.frame = rect;
    
    [UIView commitAnimations];
}


@end
