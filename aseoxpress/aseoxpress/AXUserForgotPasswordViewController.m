//
//  AXUserForgotPasswordViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/30/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXUserForgotPasswordViewController.h"
#import "AXLoginUIViewController.h"
#import "LeftMenuViewController.h"

@interface AXUserForgotPasswordViewController ()

@end

@implementation AXUserForgotPasswordViewController

@synthesize  email = _email;
@synthesize password = _password;
@synthesize passwordConfirm = _passwordConfirm;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem * resetButton = [[UIBarButtonItem alloc] initWithTitle:@"Enviar" style:UIBarButtonItemStylePlain target:self action:@selector(resetPassword:)];
    self.navigationItem.rightBarButtonItem = resetButton;
    
}

- (BOOL)checkField:(UITextField *)field{
    if(![field.text isEqual: @""]){
        return YES;
    }
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == _passwordConfirm){
        if(![self validatePasswords]){
            _passwordConfirm.text = @"";
        }
    }
}

-(BOOL)validatePasswords{
    
    if(![_password.text isEqual:_passwordConfirm.text]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                 message:@"Las contraseñas no coinciden"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                             return;
                                                         }]; //You can use a block here to handle a press on this button
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}


- (void) resetPassword:(UIBarButtonItem *)sender{
    
    
    if(![self validatePasswords]){
        return;
    }
    
    
    NSString *post = [NSString stringWithFormat:@"usuario=%@&password=%@", _email.text, _password.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/resetPassword"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data){

                                              
                                              NSError *e = nil;
                                              NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                              
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  
                                                  if([[responseDict objectForKey:@"response"]  isEqual: @"success"]){
                                                      
                                                  
                                                  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                           message:@"Contraseña cambiada exito"
                                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                                  
                                                  //We add buttons to the alert controller by creating UIAlertActions:
                                                  UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                     style:UIAlertActionStyleDefault
                                                                                                   handler:^(UIAlertAction * action){
                                                                                                       [self NavigateToAccount];
                                                                                                   }]; //You can use a block here to handle a press on this button
                                                  [alertController addAction:actionOk];
                                                  [self presentViewController:alertController animated:YES completion:nil];
                                                      
                                                      
                                                  }else{
                                                      
                                                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                               message:@"Contraseña no fue cambiada con éxito, verifique correo electrónico"
                                                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                                      
                                                      //We add buttons to the alert controller by creating UIAlertActions:
                                                      UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                         style:UIAlertActionStyleDefault
                                                                                                       handler:^(UIAlertAction * action){}]; //You can use a block here to handle a press on this button
                                                      [alertController addAction:actionOk];
                                                      [self presentViewController:alertController animated:YES completion:nil];
                                                      
                                                      
                                                  }
                                              });
                                          }
                                      }];
    [dataTask resume];

    
    
}

- (void) NavigateToAccount{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *staffViewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[storyboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    [self.navigationController presentViewController:slideNavigationController animated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
