//
//  AXStaffTableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXStaffTableViewCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel *fecha;
@property (nonatomic, strong) IBOutlet UILabel *hora;
@property (nonatomic, strong) IBOutlet UILabel *tipoServicio;
@property (nonatomic, strong) IBOutlet UITextView *notes;
@property (nonatomic, strong) IBOutlet UITextView *referencia;
@property (nonatomic, strong) IBOutlet UILabel *precio;
@property (nonatomic, strong) IBOutlet UILabel *descuento;
@property (nonatomic, strong) IBOutlet UILabel *duracion;
@property (nonatomic, strong) IBOutlet UILabel *cargosAdicional;
@property (nonatomic, strong) IBOutlet UILabel *formaPago;
@property (nonatomic, strong) IBOutlet UILabel *estatusPago;
@property (nonatomic, strong) IBOutlet UILabel *nombreEmpleado;
@property (nonatomic, strong) IBOutlet UIImageView *profilePic;
@property (nonatomic, strong) IBOutlet UIButton *evaluarStaff;
@property (nonatomic, strong) IBOutlet UIButton *paypalButton;


@end
