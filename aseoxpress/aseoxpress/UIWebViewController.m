//
//  UIWebViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/17/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "UIWebViewController.h"

@interface UIWebViewController ()

@end

@implementation UIWebViewController


@synthesize webView = _webView;
@synthesize url = _url;
@synthesize nsUrl = _nsUrl;


- (void)viewDidLoad {
    [super viewDidLoad];
    // URL set up externally
    NSURL* nsUrl = [NSURL URLWithString:_url];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
