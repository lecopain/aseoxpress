//
//  MoreTableViewController.m
//  DateCell
//
//  Created by Ajay Gautam on 3/9/14.
//
// BASED on Apple's DateCell source code
//
//

/*
 File: MyTableViewController.m
 Abstract: The main table view controller of this app.
 Version: 1.5
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */


#import "AXRegisterTableViewController.h"
#import "LeftMenuViewController.h"
#import "DatePickerTableViewCell.h"
#import "HourPickerTableViewCell.h"
#import "RatePickerTableViewCell.h"
#import "RatesTableViewCell.h"
#import <Crashlytics/Crashlytics.h>

#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view
#define kHourPickerTag              100     // view tag identifiying the date picker view
#define kRatePickerTag              101 


#define kTitleKey       @"title"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value
#define kHourKey        @"hour"
#define kRateKey        @"rate"
#define kTimePlaceholder @"Seleccione Hora"
#define kRatePlaceholder @"Seleccione Costo"


// keep track of which rows have date cells
#define kRateStartRow 1
#define kDateStartRow 3
#define kTimeStartRow 4

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end
static NSString *kTimeCellID = @"timeCell";
static NSString *kRateCellID = @"ratesCell";
static NSString *kTimePickerID = @"timePicker";
static NSString *kRatesPickerID = @"ratesPicker";
static BOOL sending = NO;

#pragma mark -

@interface AXRegisterTableViewController ()

@property (nonatomic, strong) NSArray *dataArray;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;

// keep track which indexPath points to the cell with UIPicker
@property (nonatomic, strong) NSIndexPath *hourPickerIndexPath;

@property (nonatomic, strong) NSIndexPath *ratePickerIndexPath;



@property (assign) NSInteger pickerCellRowHeight;

@property (assign) NSInteger hourpickerCellRowHeight;

@property (assign) NSInteger ratepickerCellRowHeight;



@property (nonatomic, strong) NSArray * dateRestrictions;


@property (nonatomic, strong) IBOutlet UIDatePicker *pickerView;

@property (nonatomic, retain) UIBarButtonItem *scheduleButton;

@end

@implementation AXRegisterTableViewController


@synthesize pickerView = _pickerView;
@synthesize dateRestrictions = _dateRestrictions;
@synthesize scheduleButton = _scheduleButton;

- (UITableViewCell *) createCellWithIdetifier:(NSString *)cellId {
    NSArray *reusableUiComponents = [[NSBundle mainBundle] loadNibNamed:@"ReusableUIComponents" owner:self options:nil];

    if ([kDateCellID isEqualToString:cellId]) {
        return reusableUiComponents[0];
    }
    if ([kDatePickerID isEqualToString:cellId]) {
        return reusableUiComponents[1];
    }
    if ([kOtherCell isEqualToString:cellId]) {
        return reusableUiComponents[2];
    }
    if ([kTimeCellID isEqualToString:cellId]){
        return reusableUiComponents[3];
    }
    if ([kTimePickerID isEqualToString:cellId]){
        return reusableUiComponents[4];
    }
    if ([kRatesPickerID isEqualToString:cellId]){
        return reusableUiComponents[5];
    }
    if ([kRateCellID isEqualToString:cellId]){
        return reusableUiComponents[6];
    }
    return nil;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setupDataSource {
    // setup our data source
    NSMutableDictionary *itemZero = [@{ kTitleKey : @"Confirma detalles del pedido" } mutableCopy];
    NSMutableDictionary *itemOne = [@{ kTitleKey : @"Costo", kRateKey: kRatePlaceholder} mutableCopy];
    NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Selecciona Fecha y hora" } mutableCopy];
    NSMutableDictionary *itemThree = [@{ kTitleKey : @"Fecha", kDateKey : [NSDate date] } mutableCopy];
    NSMutableDictionary *itemFour = [@{ kTitleKey : @"Hora", kHourKey: kTimePlaceholder} mutableCopy];
    NSMutableDictionary *itemFive = [@{ kTitleKey : @"" } mutableCopy];

    
    self.dataArray = @[itemZero, itemOne, itemTwo, itemThree, itemFour, itemFive];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterShortStyle];    // show short-style date format
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self createCellWithIdetifier:kDatePickerID];
//    [_pickerView setMinimumDate:[NSDate date]];
    UITableViewCell *hourPickerViewCellToCheck = [self createCellWithIdetifier:kTimePickerID];
    UITableViewCell *ratePickerViewCellToCheck = [self createCellWithIdetifier:kRatesPickerID];

    self.pickerCellRowHeight = pickerViewCellToCheck.frame.size.height;
    self.hourpickerCellRowHeight = hourPickerViewCellToCheck.frame.size.height;
    self.ratepickerCellRowHeight = ratePickerViewCellToCheck.frame.size.height;

    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupDataSource];
    
    self.title = @"Solicite servicio";
    
    
    [self.tableView setBackgroundColor:[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f]];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    _scheduleButton = [[UIBarButtonItem alloc] initWithTitle:@"Agendar" style:UIBarButtonItemStylePlain target:self action:@selector(registerService:)];
    self.navigationItem.rightBarButtonItem = _scheduleButton;
    
    self.tableView.hidden = YES;
    
    
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/agenda"]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data){
                                              
                                              NSError *e = nil;
                                              NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                              
                                              NSLog(@"%@", responseDict);
                                              _dateRestrictions = (NSMutableArray *)responseDict;
                                              self.tableView.hidden = NO;
                                          }
                                      }];
    [dataTask resume];

//    
//    _dateRestrictions =@[@{@"Id":@"224",@"fecha":@"01/01/2016",@"horas":@"TODO"},@{@"Id":@"235",@"fecha":@"29/12/2015",@"horas":@"TODO"},@{@"Id":@"227",@"fecha":@"30/12/2015",@"horas":@"TODO"}];

    


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerService:(UIBarButtonItem *)buttonItem{
    
    NSString * rate = [[self.dataArray objectAtIndex:kRateStartRow] objectForKey:kRateKey];
    
    NSDate * date1 = [[self.dataArray objectAtIndex:kDateStartRow] objectForKey:kDateKey];
    
    NSString * hour1 = [[self.dataArray objectAtIndex:kTimeStartRow] objectForKey:kHourKey];

    if([hour1 isEqualToString:kTimePlaceholder]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                 message:@"Debe seleccionar al menos una hora válida"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                         }]; //You can use a block here to handle a press on this button
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    if([rate isEqualToString:kRatePlaceholder]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                 message:@"Debe seleccionar una tarifa válida"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                         }]; //You can use a block here to handle a press on this button
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    NSDateFormatter *gmtDateFormatter = [[NSDateFormatter alloc] init];
    gmtDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-3600*6];
    gmtDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString *dateString1 = [gmtDateFormatter stringFromDate:date1];
    
    NSLog(@"%@", dateString1);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //save current userId
    NSString * userId = [defaults objectForKey:@"userId"];

    NSString *post = [NSString stringWithFormat:@"fecha1=%@&hora1=%@&idcl=%@&rate=%@", dateString1, hour1, userId, rate];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/agendarServicio"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                           _scheduleButton.enabled = NO;
                                          });
                                          NSLog(@"%@", response);
                                          NSLog(@"%@", error);
                                          if(data){
                                              
                                              NSError *e = nil;
                                              NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                              
                                              
                                              if([[responseDict objectForKey:@"response"]  isEqual: @"success"]){
                                                  
//                                                  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                  //save current userId
//                                                  NSString * userId = [responseDict objectForKey:@"id"];
//                                                  [defaults setObject:userId forKey:@"userId"];
                                                  
                                                      
                                                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                               message:@"Estamos procesando tu solicitud. Recibiras la confirmación y los datos de pago via mensaje, a tu celular"
                                                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                                      //We add buttons to the alert controller by creating UIAlertActions:
                                                      UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                         style:UIAlertActionStyleDefault
                                                                                                       handler:^(UIAlertAction * action){
                                                                                                           [self NavigateToAccount];
                                                                                                       }]; //You can use a block here to handle a press on this button
                                                      [alertController addAction:actionOk];
                                                      [self presentViewController:alertController animated:YES completion:nil];
  
                                              }
                                              else{
                                                  _scheduleButton.enabled = YES;
                                                  [CrashlyticsKit recordError:error];
                                                  
                                                  NSDictionary *userInfo = @{
                                                                             NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Date is %@",dateString1],
                                                                             NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The operation timed out.", nil),
                                                                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Have you tried turning it off and on again?", nil)
                                                                             };
                                                  NSError *error2 = [NSError errorWithDomain:NSGenericException
                                                                                       code:-57
                                                                                   userInfo:userInfo];
                                                  
                                                  [CrashlyticsKit recordError:error];
                                                  [CrashlyticsKit recordError:error2];

                                                  
                                              }
                                              
                                          }
                                      }];
    
    [dataTask resume];

    
    
    
//    NSDate *dte = [dateFormat dateFromString:str];

}

- (void) NavigateToAccount{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *staffViewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"staffScreen"];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[storyboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    [self.navigationController presentViewController:slideNavigationController animated:NO completion:nil];
    
}

#pragma mark - Utilities

/*! Returns the major version of iOS, (i.e. for iOS 6.1.3 it returns 6)
 */
NSUInteger UNUSED_DeviceSystemMajorVersion() // TODO - move this to Utils
{
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _deviceSystemMajorVersion = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    });
    
    return _deviceSystemMajorVersion;
}

/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Determines if the given indexPath has a cell below it with a UIPicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasHourPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasHourPicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kHourPickerTag];
    
    hasHourPicker = (checkDatePicker != nil);
    return hasHourPicker;
}

//Rates
- (BOOL)hasRatePickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasRatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIPickerView *checkDatePicker = (UIPickerView *)[checkDatePickerCell viewWithTag:kRatePickerTag];
    
    hasRatePicker = (checkDatePicker != nil);
    return hasRatePicker;
}


- (NSString *) resolveAvailabeDate:(NSIndexPath *)indexPath{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    //IndexPath is 2 positions away from Date Cell
    NSDate * dateToEvaluate = [[self.dataArray objectAtIndex:indexPath.row - 2] objectForKey:kDateKey];
//    NSTimeInterval timeZoneSeconds = -3600*6;
//    NSDate * dateToEvaluateInTimeZone = [dateToEvaluate dateByAddingTimeInterval:timeZoneSeconds];
    
    [calendar setTimeZone: [NSTimeZone timeZoneForSecondsFromGMT:-3600*6]];

    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: dateToEvaluate];
    NSDate * date1 = [calendar dateFromComponents:date1Components];
    
    
    for(int i = 0; i < _dateRestrictions.count ; i++){
        NSString * restrictedStringDate = [[_dateRestrictions objectAtIndex:i] objectForKey:@"fecha"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-3600*6]];
        [dateFormatter setCalendar:calendar];
        
        NSDate * restrictedDate = [[NSDate alloc] init];
        restrictedDate = [dateFormatter dateFromString:restrictedStringDate];
        
        NSDateComponents *date2Components = [calendar components:comps
                                                        fromDate: restrictedDate];
        NSDate * date2 = [calendar dateFromComponents:date2Components];
        
        
        NSLog(@"%@", restrictedDate);
        
        switch ([date1 compare:date2]) {
            case NSOrderedSame:
                return [[_dateRestrictions objectAtIndex:i] objectForKey:@"horas"];
                break;
            default:
                break;
        }
    }
    return nil;
}


/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateHourPicker
{
    if (self.hourPickerIndexPath != nil)
    {
        HourPickerTableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.hourPickerIndexPath];
        
//        HourPickerTableViewCell *targetedUIPickerView = (HourPickerTableViewCell *)[associatedDatePickerCell viewWithTag:kHourPickerTag];
        if (associatedDatePickerCell != nil)
        {
            NSString *restriction = [self resolveAvailabeDate:self.hourPickerIndexPath];
            if([restriction isEqualToString:@"TODO"]){
                [associatedDatePickerCell setAvailabilityNone];
                [self HourAtIndex:kTimePlaceholder];
            }
            else if([restriction isEqualToString:@"AM"]){
                [associatedDatePickerCell setAvailabilityPM];
            }
            else if([restriction isEqualToString:@"PM"]){
                [associatedDatePickerCell setAvailabilityAM];
            }
            else{
                [associatedDatePickerCell setAvailabilityAll];
            }

            
            // we found a UIPicker in this cell, so update it's date value
            //
//            NSDictionary *itemData = self.dataArray[self.hourPickerIndexPath.row - 1];

            //[targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
            
            // set the call action for the date picker to dateAction
            //[targetedUIPickerView addTarget:self action:@selector(dateAction:) forControlEvents:UIControlEventValueChanged];
        }
    }
}

- (void)updateRatePicker
{
    if (self.ratePickerIndexPath != nil)
    {
         RatePickerTableViewCell *associatedRatePickerCell = [self.tableView cellForRowAtIndexPath:self.ratePickerIndexPath];
        
        //        HourPickerTableViewCell *targetedUIPickerView = (HourPickerTableViewCell *)[associatedDatePickerCell viewWithTag:kHourPickerTag];
        if (associatedRatePickerCell != nil)
        {
              [self RateAtIndex:kRatePlaceholder];
//              [associatedRatePickerCell addTarget:self action:@selector(dateAction:) forControlEvents:UIControlEventValueChanged];
//            [associatedRatePickerCell setRateAvailability];
        }
            
            
            // we found a UIPicker in this cell, so update it's date value
            //
            //            NSDictionary *itemData = self.dataArray[self.hourPickerIndexPath.row - 1];
            
            //[targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
            
            // set the call action for the date picker to dateAction
            //[targetedUIPickerView addTarget:self action:@selector(dateAction:) forControlEvents:UIControlEventValueChanged];
    }
}


/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            NSDictionary *itemData = self.dataArray[self.datePickerIndexPath.row - 1];
            [targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
            
            // set the call action for the date picker to dateAction
            [targetedDatePicker addTarget:self action:@selector(dateAction:) forControlEvents:UIControlEventValueChanged];
        }
    }
   
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    return (self.datePickerIndexPath != nil);
}
/*! Determines if the UITableViewController has a UIPicker in any of its cells.
 */
- (BOOL)hasInlineHourPicker
{
    return (self.hourPickerIndexPath != nil);
}

- (BOOL)hasInlineRatePicker
{
    return (self.ratePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the UIPicker for hours.
 
 @param indexPath The indexPath to check if it represents a cell with the UIPicker for hours.
 */

- (BOOL)indexPathHasHourPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineHourPicker] && self.hourPickerIndexPath.row == indexPath.row);
}
/*! Determines if the given indexPath points to a cell that contains the UIPicker for rates.
 
 @param indexPath The indexPath to check if it represents a cell with the UIPicker for rates.
 */

- (BOOL)indexPathPointsRatePicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineRatePicker] && self.ratePickerIndexPath.row == indexPath.row);
}


/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) || ([self hasInlineDatePicker] ))
    {
        hasDate = YES;
    }
    
    return hasDate;
}
- (BOOL)indexPathHasTimePicker:(NSIndexPath *)indexPath
{
    BOOL hasTimePicker = NO;
    
    if ((indexPath.row == kTimeStartRow))
    {
        hasTimePicker = YES;
    }
    
    return hasTimePicker;
}

- (BOOL)indexPathHasRatePicker:(NSIndexPath *)indexPath
{
    BOOL hasRatePicker = NO;
    
    if ((indexPath.row == kRateStartRow))
    {
        hasRatePicker = YES;
    }
    
    return hasRatePicker;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    
    if([self indexPathHasPicker:indexPath]){
        height = self.pickerCellRowHeight;
    }
    else if([self indexPathHasHourPicker:indexPath]){
        height = self.hourpickerCellRowHeight;
    }
    else if([self indexPathPointsRatePicker:indexPath]){
        height = self.ratepickerCellRowHeight;
    }
    else{
        height = self.tableView.rowHeight;
    }

    return height;
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self hasInlineDatePicker] || [self hasInlineHourPicker] || [self hasInlineRatePicker])
    {
        // we have a date picker, so allow for it in the number of rows in this section
        NSInteger numRows = self.dataArray.count;
        return ++numRows;
    }
    
    
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSString *cellID = kOtherCell;
    
    if ([self indexPathHasPicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kDatePickerID;     // the current/opened date picker cell
    }
    else if ([self indexPathHasDate:indexPath])
    {
        // the indexPath is one that contains the date information
        cellID = kDateCellID;       // the start/end date cells
    }
    else if( [self indexPathHasTimePicker: indexPath]){
        // this cell is a non-date cell, just assign it's text label
        cellID = kTimeCellID;
    }
    else if( [self indexPathHasRatePicker: indexPath]){
        // this cell is a non-date cell, just assign it's text label
        cellID = kRateCellID;
    }
    else if ([self indexPathHasHourPicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kTimePickerID;     // the current/opened date picker cell
    }
    else if ([self indexPathPointsRatePicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kRatesPickerID;     // the current/opened date picker cell
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        cell = [self createCellWithIdetifier:cellID];
        
    }
    //if a date cell restrict minimum date
    if(cellID == kDatePickerID){
        DatePickerTableViewCell * dateCell = (DatePickerTableViewCell *)[self createCellWithIdetifier:cellID];
        [[dateCell datePickerElement] setMinimumDate:[NSDate date]];
        cell = dateCell;
          NSLog(@"%@", cell);
    }
    if(cellID == kTimePickerID){
        [(HourPickerTableViewCell *)cell setDataSource:self];
    }
    
    if(cellID == kRatesPickerID){
        [(RatePickerTableViewCell *)cell setDelegate:self];
    }
    
    if (indexPath.row == 0)
    {
        // we decide here that first cell in the table is not selectable (it's just an indicator)
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // if we have a date picker open whose cell is above the cell we want to update,
    // then we have one more cell than the model allows
    //
    NSInteger modelRow = indexPath.row;
    if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row)
    {
        modelRow--;
    }
    
    NSDictionary *itemData = self.dataArray[modelRow];
    // proceed to configure our cell
    if ([cellID isEqualToString:kDateCellID])
    {
        // we have either start or end date cells, populate their date field
        //
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.detailTextLabel.text = [self.dateFormatter stringFromDate:[itemData valueForKey:kDateKey]];
    }
    else if ([cellID isEqualToString:kOtherCell])
    {
        // this cell is a non-date cell, just assign it's text label
        //
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    }else if([cellID isEqualToString:kTimeCellID] || [cellID isEqualToString:kRateCellID]){
        //cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.backgroundColor = [UIColor whiteColor];
    }

    
    return cell;
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Adds or removes a UIPickerView cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIPicker.
 */
- (void)toggleHourPickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasHourPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}
//rates
- (void)toggleRatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasRatePickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    
    if ([self hasInlineRatePicker])
    {
        before = self.ratePickerIndexPath.row < indexPath.row;
    }
    else if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    else if ([self hasInlineHourPicker])
    {
        before = self.hourPickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineRatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.ratePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.ratePickerIndexPath = nil;
    }
    if ([self hasInlineHourPicker])
    {
        NSLog(@"INDEX PATH %@", [NSIndexPath indexPathForRow:self.hourPickerIndexPath.row inSection:0]);
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.hourPickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.hourPickerIndexPath = nil;
    }
    if ([self hasInlineDatePicker])
    {
        NSLog(@"INDEX PATH %@", [NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]);
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }
    
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
            [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
            self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
//
//            [self toggleHourPickerForSelectedIndexPath:indexPathToReveal];
//            self.hourPickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}

/*! Reveals the hour picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIPicker.
 */
- (void)displayInlineHourPickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineRatePicker])
    {
        before = self.ratePickerIndexPath.row < indexPath.row;
    }
    else if ([self hasInlineHourPicker])
    {
        before = self.hourPickerIndexPath.row < indexPath.row;
    }
    else if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    
    BOOL sameCellClicked = (self.hourPickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineRatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.ratePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.ratePickerIndexPath = nil;
    }
    else if ([self hasInlineHourPicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.hourPickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.hourPickerIndexPath = nil;
    }
    else if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
         NSLog(@"INDEX PATH %@", [NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]);
         
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleHourPickerForSelectedIndexPath:indexPathToReveal];
        self.hourPickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateHourPicker];
}
//Rates
- (void)displayInlineRatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineRatePicker])
    {
        before = self.ratePickerIndexPath.row < indexPath.row;
    }
    
    
    BOOL sameCellClicked = (self.ratePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    // remove any date picker cell if it exists
    if ([self hasInlineRatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.ratePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.ratePickerIndexPath = nil;
    }
    if ([self hasInlineHourPicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.hourPickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.hourPickerIndexPath = nil;
    }
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        NSLog(@"INDEX PATH %@", [NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]);
        
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleRatePickerForSelectedIndexPath:indexPathToReveal];
        self.ratePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateRatePicker];
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([kDateCellID isEqualToString:cell.reuseIdentifier])
    {
        [self displayInlineDatePickerForRowAtIndexPath:indexPath];
    }
    else if ([kTimeCellID isEqualToString:cell.reuseIdentifier]){
        
        [self displayInlineHourPickerForRowAtIndexPath:indexPath];
    }
    else if ([kRateCellID isEqualToString:cell.reuseIdentifier]){
        
        [self displayInlineRatePickerForRowAtIndexPath:indexPath];
    }
    
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Actions

/*! User chose to change the hour by changing the values inside the UIPicker.
*/


- (void) HourAtIndex:(NSString *)selectedHour{
    
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineHourPicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.hourPickerIndexPath.row - 1 inSection:0];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    //UIDatePicker *targetedDatePicker = sender;
    
    
    
    // update our data model
    NSMutableDictionary *itemData = self.dataArray[targetedCellIndexPath.row];
    [itemData setValue:selectedHour forKey:kHourKey];
    
    // update the cell's date string
    cell.detailTextLabel.text = selectedHour;

    
    
}
/*! User chose to change the cost by changing the values inside the UIPicker.
 */


- (void) RateAtIndex:(NSString *)selectedRate{
    
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineRatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.ratePickerIndexPath.row - 1 inSection:0];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    RatesTableViewCell *cell = (RatesTableViewCell *)[self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    //UIDatePicker *targetedDatePicker = sender;
    
    
    
    // update our data model
    NSMutableDictionary *itemData = self.dataArray[targetedCellIndexPath.row];
    [itemData setValue:selectedRate forKey:kRateKey];
    
    // update the cell's date string
    cell.rateLabel.text = selectedRate;
    
    
    
}

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (void)dateAction:(id)sender
{
    [self updateHourPicker];
    
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:0];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    
    
    // update our data model
    NSMutableDictionary *itemData = self.dataArray[targetedCellIndexPath.row];
    [itemData setValue:targetedDatePicker.date forKey:kDateKey];
    
    // update the cell's date string
    cell.detailTextLabel.text = [self.dateFormatter stringFromDate:targetedDatePicker.date];
}


- (void)datePickerChanged
{
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:datePicker.date];
//    
//    if([components hour] < 7)
//    {
//        [components setHour:7];
//        [components setMinute:0];
//        [datePicker setDate:[[NSCalendar currentCalendar] dateFromComponents:components]];
//    }
//    else if([components hour] > 21)
//    {
//        [components setHour:21];
//        [components setMinute:59];
//        [datePicker setDate:[[NSCalendar currentCalendar] dateFromComponents:components]];
//    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
