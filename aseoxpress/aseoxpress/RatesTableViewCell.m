//
//  RatesTableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 4/15/16.
//  Copyright © 2016 Fernando Jimenez. All rights reserved.
//

#import "RatesTableViewCell.h"

@implementation RatesTableViewCell

@synthesize rateLabel = _rateLabel;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
