//
//  TimeCostTableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 4/9/16.
//  Copyright © 2016 Fernando Jimenez. All rights reserved.
//

#import "RatePickerTableViewCell.h"

@implementation RatePickerTableViewCell

@synthesize ratePickerElement = _ratePickerElement;
@synthesize delegate = _delegate;
@synthesize rates = _rates;
@synthesize ratesMutableArray = _ratesMutableArray;

- (void)awakeFromNib {
    
    //    NSLog(@"AWAKEN");
    [_ratePickerElement setDelegate:self];
    [_ratePickerElement setDataSource:self];
    
    // Initialization code
    _rates = @[@"3 horas, $265", @"4 horas, $315", @"5 horas, $415", @"2 pers, 3 horas - $530", @"2 pers, 4 horas - $630"];
    _ratesMutableArray = (NSMutableArray *)_rates;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _rates.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _rates[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"%@", _rates[row]);
    [_delegate RateAtIndex:_rates[row]];
}







@end