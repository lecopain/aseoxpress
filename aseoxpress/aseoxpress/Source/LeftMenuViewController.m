//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "LoginNavigationViewController.h"
#import "AXStaffTableViewController.h"
#import "AXLoginUIViewController.h"
#import "AppDelegate.h"
#import "UIWebViewController.h"

@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
  	self.tableView.separatorColor = [UIColor lightGrayColor];
	
//	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
//	self.tableView.backgroundView = imageView;
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int loggedIn = [[defaults objectForKey:@"userId"] intValue ];
    
    if(loggedIn){
        return 6;
    }
    else{
        return 5;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
	view.backgroundColor = [UIColor clearColor];
	return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
	
	switch (indexPath.row)
	{
		case 0:
			cell.textLabel.text = @"Preguntas Frecuentes";
			break;
			
		case 1:
			cell.textLabel.text = @"Tarifas";
			break;
			
		case 2:
			cell.textLabel.text = @"Términos y Condiciones";
			break;
        case 3:
            cell.textLabel.text = @"aseoxpress.tv";
            break;
        case 4:
            cell.textLabel.text = @"Contacto";
            break;
        case 5:
            cell.textLabel.text = @"Cerrar Sesión";
            break;
	}
	
	cell.backgroundColor = [UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int loggedIn = [[defaults objectForKey:@"userId"] intValue ];
//    
//    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
//    AXStaffTableViewController *staffViewController = (AXStaffTableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"staffScreen"];
//    
//    AXLoginUIViewController *loginViewController = (AXLoginUIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
//    
//    
    LeftMenuViewController *leftMenu;
//
//    
    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];

    UIWebViewController *webView = (UIWebViewController*)[storyboard
                                                                 instantiateViewControllerWithIdentifier: @"WebViewController"];
    
    SlideNavigationController *slideNavigationController;
    
    //    if(loggedIn){
    //            slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
    //
    //    }
    //    else{
    //            slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: loginViewController];
    //    }

    
    NSString* url = @"http://www.aseoxpress.com/directorvirtual/movil";
	
	switch (indexPath.row)
	{
		case 0:
            url = @"http://www.aseoxpress.com/directorvirtual/movil/index.php?op=preguntas&isapp=yes";
            [webView setUrl:url];


            
            [[SlideNavigationController sharedInstance] pushViewController:webView animated:NO];
            
            break;
			
		case 1:
            url = @"http://www.aseoxpress.com/directorvirtual/movil/index.php?op=tarifasdx&isapp=yes";

            [webView setUrl:url];
            
            [[SlideNavigationController sharedInstance] pushViewController:webView animated:NO];
            
            return;
            break;
		case 2:
            url = @"http://www.aseoxpress.com/directorvirtual/movil/index.php?op=terminos&isapp=yes";
            
            [webView setUrl:url];
            
            [[SlideNavigationController sharedInstance] pushViewController:webView animated:NO];
            
            return;
            break;
			
		case 3:
            
            url = @"http://aseoxpress.tv";
            
            [webView setUrl:url];
            
            [[SlideNavigationController sharedInstance] pushViewController:webView animated:YES];


            break;
        case 4:
            
            url = @"http://www.aseoxpress.com/directorvirtual/movil/index.php?op=contacto&isapp=yes";
            
            [webView setUrl:url];
            
            [[SlideNavigationController sharedInstance] pushViewController:webView animated:YES];
            
            return;
            break;
            
        case 5:
            
//            vc = [storyboard instantiateViewControllerWithIdentifier: @"loginScreen"];
////            [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:vc withCompletion:nil];
//            
//            AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
//            
//            UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"loginScreen"];
//            
//            UINavigationController* navigation = [[UINavigationController alloc] initWithRootViewController:rootController];
//            appDelegateTemp.window.rootViewController = navigation;
//            return;
            
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
            [self.tableView reloadData];
//            leftMenu = (LeftMenuViewController*)[storyboard
//                                                    instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];


            AXLoginUIViewController *loginViewController = (AXLoginUIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
            
            leftMenu = (LeftMenuViewController*)[storyboard instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
            
            slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: loginViewController];

    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    
    appDelegateTemp.window.rootViewController = [SlideNavigationController sharedInstance];

        break;
    
    
	}
	

}

@end
