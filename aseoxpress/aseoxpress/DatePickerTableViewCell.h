//
//  DatePickerTableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/12/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIDatePicker* datePickerElement;

@end
