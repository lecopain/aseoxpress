//
//  ViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface AXLoginUIViewController : UIViewController<SlideNavigationControllerDelegate, UITextFieldDelegate>


    @property (nonatomic, strong) IBOutlet UITextField * userUITextField;
    @property (nonatomic, strong) IBOutlet UITextField * passwordUITextField;
    @property (nonatomic, strong) IBOutlet UIScrollView * scrollView;
    @property (nonatomic, strong) IBOutlet UIImageView * imageView;

-(IBAction)registerUser:(id)sender;
-(IBAction)userForgotPassword:(id)sender;
- (IBAction)login:(id)sender;


@end

