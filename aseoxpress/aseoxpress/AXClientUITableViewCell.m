//
//  AXClientUITableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 11/20/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXClientUITableViewCell.h"

@implementation AXClientUITableViewCell

@synthesize userName, email, phone, address, rooms, bathrooms;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
