//
//  AXStaffTableViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXStaffTableViewController.h"
#import "AXStaffTableViewCell.h"
#import "LeftMenuViewController.h"
#import "AXClientUITableViewCell.h"
#import "AXRegisterTableViewController.h"
#import "AXRatingsUIViewController.h"
#import "PayPalMobile.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

@interface AXStaffTableViewController ()

@end

@implementation AXStaffTableViewController

@synthesize tableView = _tableView;
@synthesize tableData = _tableData;
@synthesize jsonObjects = _jsonObjects;
@synthesize staffImages = _staffImages;
@synthesize estrellas = _estrellas;
@synthesize estrellasPicker = _estrellasPicker;
@synthesize serverResponse = _serverResponse;
@synthesize userId = _userId;
@synthesize currentPrice = _currentPrice;
@synthesize existente = _existente;
@synthesize selectedPosition = _selectedPosition;

#pragma mark - Paypal processing

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _payPalConfiguration = [[PayPalConfiguration alloc] init];
        
        // See PayPalConfiguration.h for details and default values.
        // Should you wish to change any of the values, you can do so here.
        // For example, if you wish to accept PayPal but not payment card payments, then add:
        _payPalConfiguration.acceptCreditCards = NO;

        // Or if you wish to have the user choose a Shipping Address from those already
        // associated with the user's PayPal account, then add:
        _payPalConfiguration.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    }
    return self;
}

- (IBAction)pay:(UIButton *)button{
    
    // Create a PayPalPayment
    PayPalPayment *payment = [[PayPalPayment alloc] init];

    
    // Amount, currency, and description
    NSArray *programacion = [_serverResponse objectForKey:@"programacion"];
    NSDictionary * selectedRow = [programacion objectAtIndex:button.tag];
    int amountWithPayPalComission = [[selectedRow objectForKey:@"costo"] floatValue]*1.04;
    payment.amount = [[NSDecimalNumber alloc] initWithInt:amountWithPayPalComission];
    payment.currencyCode = @"MXN";
    payment.shortDescription = @"Servicio de limpieza";
    payment.invoiceNumber = [selectedRow objectForKey:@"Id"];
    
    
    // Use the intent property to indicate that this is a "sale" payment,
    // meaning combined Authorization + Capture.
    // To perform Authorization only, and defer Capture to your server,
    // use PayPalPaymentIntentAuthorize.
    // To place an Order, and defer both Authorization and Capture to
    // your server, use PayPalPaymentIntentOrder.
    // (PayPalPaymentIntentOrder is valid only for PayPal payments, not credit card payments.)
    payment.intent = PayPalPaymentIntentSale;
    
    // If your app collects Shipping Address information from the customer,
    // or already stores that information on your server, you may provide it here.
    //payment.shippingAddress = address; // a previously-created PayPalShippingAddress object
    
    // Several other optional fields that you can set here are documented in PayPalPayment.h,
    // including paymentDetails, items, invoiceNumber, custom, softDescriptor, etc.
    
    // Check whether payment is processable.
    if (!payment.processable) {
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
    }
    
    // Create a PayPalPaymentViewController.
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                   configuration:self.payPalConfiguration
                                                                        delegate:self];

    
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    // Send the entire confirmation dictionary
//    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
//                                                           options:0
//                                                             error:nil];
    
    
    NSString *post = [NSString stringWithFormat:@"servicio_id=%@",completedPayment.invoiceNumber];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/actualizaPago"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data){
                                          
                                              NSError *e = nil;
                                              NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                              
                                              NSLog(@"%@", responseDict);
                                          
                                          }
                                      }];
    [dataTask resume];
    
    
    
    
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
}






- (void)viewWillAppear:(BOOL)animated{
    
    
    // Start out working with the test environment! When you are ready, switch to PayPalEnvironmentProduction.
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];

    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    

    NSString *post = [NSString stringWithFormat:@"usuario_id=%@",_userId];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/listarCliente"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data){
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  
                                                  NSError *e = nil;
                                                  NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
                                                  
                                                  NSLog(@"%@", responseDict);
                                                  _serverResponse = responseDict;
                                                  
                                                  NSString * existente = [_serverResponse objectForKey:@"existente"];
                                                  
                                                  if([existente isEqual:@"NO"]){
                                                      UIBarButtonItem *agendarButton = [[UIBarButtonItem alloc] initWithTitle:@"+ Solicitar Servicio" style:UIBarButtonItemStylePlain target:self action:@selector(addStaff:)];
                                                      self.navigationItem.rightBarButtonItem = agendarButton;
                                                      _existente = @"NO";
                                                  }
                                                  else{
                                                      _existente = @"YES";
                                                  }
                                                  
                                                  /* Your UI code */
                                                  [_tableView reloadData];

                                              });
                                          }
                                          else{
                                              NSLog(@"%@", response);
                                              NSLog(@"%@", error);
                                          }
                                      }];
    [dataTask resume];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    [self viewWillAppear:NO];
    [refreshControl endRefreshing];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
//    [PayPal getPayPalInst];
    //UIButton *button = [[PayPal getPayPalInst] getPayButtonWithTarget:self andAction:action andButtonType:type];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //save current userId

    _serverResponse = nil;
    _userId = [defaults objectForKey:@"userId"];
    _selectedPosition = 0;

    
//    UIBarButtonItem *plusButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addStaff:)];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)addStaff:(UIBarButtonItem *)sender{
    
    AXRegisterTableViewController *vc = [AXRegisterTableViewController new];
    [self.navigationController pushViewController:vc animated:YES];    
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section==0)
    {
        return 1;
    }
    else{
        if(_serverResponse){
            int count = 0;
            NSArray *programacion = [_serverResponse objectForKey:@"programacion"];
            if([programacion count] > 0){ 
                count += [programacion count];
            }
            if([_existente isEqual:@"YES"]){
                count += 1;
            }
            return count;
        }
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return @"Perfil de cliente";
    else
        return @"Detalle de servicios";
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == 0){
        return 125.00;
    }
    else if ( IDIOM == IPAD ) {
        return 240.00;
    } else {
        return 312.00;
    }
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"";
    
    UITableViewCell *cell;

    if (indexPath.section == 0){
        simpleTableIdentifier = @"AXUIClientViewCell";
        
       cell = (AXClientUITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        
        if (cell == nil){
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            if(_serverResponse){
                NSDictionary * clientInfo = [_serverResponse objectForKey:@"cliente"];
                [[(AXClientUITableViewCell *)cell userName] setText:[clientInfo objectForKey:@"nombre"]];
                [[(AXClientUITableViewCell *)cell email] setText:[clientInfo objectForKey:@"correo"]];
                [[(AXClientUITableViewCell *)cell phone] setText:[clientInfo objectForKey:@"celular"]];
                [[(AXClientUITableViewCell *)cell address] setText:[clientInfo objectForKey:@"domicilio"]];
                [[(AXClientUITableViewCell *)cell rooms] setText:[clientInfo objectForKey:@"norecamaras"]];
                [[(AXClientUITableViewCell *)cell bathrooms] setText:[clientInfo objectForKey:@"nobanos"]];
            }
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else{
        
        // si la solicitud está en proceso
        if([_existente isEqual:@"YES"] && indexPath.row == 0){
            cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AXUITableViewCellMessage"];
            if (cell == nil){
                
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AXUITableViewCellMessage" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        
        //tipo de pantalla
    
        if ( IDIOM == IPAD ) {
            simpleTableIdentifier = @"AXUITableViewCell";
        } else {
            simpleTableIdentifier =  @"AXUITableViewCelliPhone";
        }
        
        
        cell = (AXStaffTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil){
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
            
            if(_serverResponse){
                
                NSInteger realIndexPath = indexPath.row;
                if([_existente isEqual:@"YES"]){
                    realIndexPath -= 1;
                }
                
                
                NSArray *programacion = [_serverResponse objectForKey:@"programacion"];
                
                
                if([programacion count] > 0){
                    
                    NSString *ImageURL = [[programacion objectAtIndex:realIndexPath] objectForKey:@"foto"];
                    NSString * estatus = [[programacion objectAtIndex:realIndexPath] objectForKey:@"estatus"];
                    
//                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//                    [[(AXStaffTableViewCell *)cell profilePic] setImage:[UIImage imageWithData:imageData]];
//                    [[(AXStaffTableViewCell *)cell profilePic] setContentMode:UIViewContentModeScaleAspectFit];
//                    
                    [[(AXStaffTableViewCell *)cell profilePic] sd_setImageWithURL:[NSURL URLWithString:ImageURL] placeholderImage:[UIImage imageNamed:@"loginedo.jpg"]];
                    
                    [[(AXStaffTableViewCell *)cell fecha] setText: [[programacion objectAtIndex:realIndexPath] objectForKey:@"fecha"]];
                    [[(AXStaffTableViewCell *)cell hora] setText: [[programacion objectAtIndex:realIndexPath] objectForKey:@"hora"]];
                    [[(AXStaffTableViewCell *)cell tipoServicio] setText: [[programacion objectAtIndex:realIndexPath] objectForKey:@"tiposerv"]];
                    [[(AXStaffTableViewCell *)cell duracion] setText: [NSString stringWithFormat:@"%@ Hrs", [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"duracion"]]]];
                    
                    [[(AXStaffTableViewCell *)cell paypalButton] addTarget:self action:@selector(pay:) forControlEvents:UIControlEventTouchUpInside];
                    [[(AXStaffTableViewCell *)cell paypalButton] setTag:realIndexPath];
                    [[[(AXStaffTableViewCell *)cell paypalButton] layer] setCornerRadius:12.0f];
                    
                    [[[(AXStaffTableViewCell *)cell paypalButton] layer] setMasksToBounds:YES];
                    
                    [[[(AXStaffTableViewCell *)cell paypalButton] layer] setBorderWidth:1.0f];
                    
                    [[[(AXStaffTableViewCell *)cell paypalButton] layer] setBorderColor:[ [ UIColor whiteColor ] CGColor ]];
                    
                     
                    if([estatus isEqualToString:@"APLICADO"] || [estatus isEqualToString:@"CANCELADA"]){
                        [[(AXStaffTableViewCell *)cell paypalButton] setEnabled:NO];
                        [[(AXStaffTableViewCell *)cell paypalButton] setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
                    }
                     else{
                         [[[(AXStaffTableViewCell *)cell paypalButton] layer] setBorderColor:[ [ UIColor grayColor ] CGColor ]];
                     }


                    
                    [[(AXStaffTableViewCell *)cell notes] setText: [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"notas"]]];
                    [[(AXStaffTableViewCell *)cell referencia] setText:[self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"referencia"]]];
                    [[(AXStaffTableViewCell *)cell precio] setText: [NSString stringWithFormat:@"$%@", [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"costo"]]]];
                    [[(AXStaffTableViewCell *)cell formaPago] setText: [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"formadepa"]]];
                    
                    [[(AXStaffTableViewCell *)cell descuento] setText: [NSString stringWithFormat:@"$%.1f", [[self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"descu"]] floatValue]]];
                    
                    [[(AXStaffTableViewCell *)cell cargosAdicional] setText: [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"cargo"]]];

                    
    

                    [[(AXStaffTableViewCell *)cell estatusPago] setText: [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"estatus"]]];
                    if([estatus isEqualToString:@"PENDIENTE"]){
                        [[(AXStaffTableViewCell *)cell estatusPago] setTextColor:[UIColor redColor]];
                    }
                    if([estatus isEqualToString:@"CANCELADA"]){
                        [[(AXStaffTableViewCell *)cell estatusPago] setTextColor:[UIColor blackColor]];
                    }
                    
                    
                    
                    [[(AXStaffTableViewCell *)cell nombreEmpleado] setText: [self resolveValue:[[programacion objectAtIndex:realIndexPath] objectForKey:@"alias"]]];
                    
                    UIButton * evButton = [(AXStaffTableViewCell *)cell evaluarStaff];
                    evButton.tag = realIndexPath;
                    NSLog(@"%ld", (long)realIndexPath);
                    NSLog(@"%ld", (long)evButton.tag);
                    [evButton addTarget:self action:@selector(evaluarStaff:)
                forControlEvents:UIControlEventTouchUpInside];
                    [(AXStaffTableViewCell *)cell setEvaluarStaff:evButton];


                    
                }

           
                
                
            }
            
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
         return cell;
        
    }
    return nil;
}

- (void) evaluarStaff:(UIButton *)sender{
    
    _selectedPosition = sender.tag;
    [self performSegueWithIdentifier:@"ratingsSegue" sender:self];

    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"ratingsSegue"]) {
        
        NSArray *programacion = [_serverResponse objectForKey:@"programacion"];
        NSDictionary * selectedRow = [programacion objectAtIndex:_selectedPosition];
        NSLog(@"%@", selectedRow);
        
       
        AXRatingsUIViewController *myVC = [segue destinationViewController];
        myVC.userAlias = [selectedRow objectForKey:@"alias"];
        myVC.idEmpleado = [selectedRow objectForKey:@"idempleado"];
       
    }
}


- (NSString *) resolveValue:(NSString *)possibleValue{
    if(![possibleValue isEqual: [NSNull null]]){
        return possibleValue;
    }
    else{
        return @"";
    }
}






/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
