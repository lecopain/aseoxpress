//
//  AppDelegate.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LeftMenuViewController.h"
#import "AXLoginUIViewController.h"
#import "AXStaffTableViewController.h"
#import "LoginNavigationViewController.h"
#import "SlideNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PayPalMobile.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int loggedIn = [[defaults objectForKey:@"userId"] intValue ];
    
    
    //FBSDK initialization
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    //PayPal Initialization
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction :@"AUD_2GhWt6Rfdtr2lx_N2HdMQkIJ4OrlMVOot6SRmR0aYWgaazf2xFH1gb6MSUU76hPwinsswM95J_DG",
                                                           PayPalEnvironmentSandbox : @"AQ34aJg9XWhwIhTBdHlJOxP1_GBmML0sT-w5qHP-De45CIdzy5T_j5vaqHA9JKw6nFvB6dpowclpFdw4"}];
    

    


        
    //  YourViewControllerClass *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"];
        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        LoginNavigationViewController *loginViewController = (LoginNavigationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginNavigation"];
////        [self popViewControllerAnimated:YES];
//        [self.window makeKeyAndVisible];
//        [self.window.rootViewController presentViewController:loginViewController animated:NO completion:NULL];
    
    
        // Do any additional setup after loading the view.
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        LeftMenuViewController *leftMenu;
        SlideNavigationController *slideNavigationController;
    
        if (loggedIn){
              AXStaffTableViewController *registerController = (AXStaffTableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"staffScreen"];
              leftMenu = (LeftMenuViewController*)[storyboard instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
               slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: registerController];

        }
        else{
        AXLoginUIViewController *loginViewController = (AXLoginUIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"loginScreen"];
            
            leftMenu = (LeftMenuViewController*)[storyboard instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
            
            slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: loginViewController];
        
        }
       
        [SlideNavigationController sharedInstance].leftMenu = leftMenu;
        [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
        [SlideNavigationController sharedInstance].enableShadow = YES;
        [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
        [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
        
        
        self.window.rootViewController = [SlideNavigationController sharedInstance];
        return YES;

}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
