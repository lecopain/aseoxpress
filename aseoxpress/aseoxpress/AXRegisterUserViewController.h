//
//  AXRegisterUserViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/26/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AXRegisterUserViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) NSArray *municipio;
@property (strong, nonatomic) NSArray *formaPago;
@property (strong, nonatomic) NSArray *recamaras;
@property (strong, nonatomic) NSArray *banios;
@property (strong, nonatomic) NSArray *medio;

//textfields
@property (strong, nonatomic) IBOutlet UITextField *nombreCompleto;
@property (strong, nonatomic) IBOutlet UITextField *celular;
@property (strong, nonatomic) IBOutlet UITextField *domicilio;
@property (strong, nonatomic) IBOutlet UITextField *colonia;
@property (strong, nonatomic) IBOutlet UITextField *referencias;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *contrasena;
@property (strong, nonatomic) IBOutlet UITextField *confirmarContrasena;


@property (strong, nonatomic) IBOutlet UITextField *citySelectorUITextField;
@property (strong, nonatomic) IBOutlet UITextField *paymentSelectorUITextField;
@property (strong, nonatomic) IBOutlet UITextField *contactMediumUITextField;
@property (strong, nonatomic) IBOutlet UITextField *roomSelectorUITextField;
@property (strong, nonatomic) IBOutlet UITextField *bathroomSelectorUITextField;


@property (strong, nonatomic) IBOutlet UILabel *costLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;

//UIPickers
@property (strong, nonatomic) IBOutlet UIPickerView *cityPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *paymentpickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *roomPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *bathPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *mediumPickerView;


-(IBAction)registrarUsuario:(id)sender;
- (NSDictionary *) calcCostos:(float)NumeroBanos recamaras:(int)NumeroRecamaras;


@end
