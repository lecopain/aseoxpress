//
//  AXClientUITableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 11/20/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXClientUITableViewCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel * userName;
@property (nonatomic, strong) IBOutlet UILabel * email;
@property (nonatomic, strong) IBOutlet UILabel * phone;
@property (nonatomic, strong) IBOutlet UITextView * address;
@property (nonatomic, strong) IBOutlet UILabel * rooms;
@property (nonatomic, strong) IBOutlet UILabel * bathrooms;

@end
