//
//  AXUserForgotPasswordViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/30/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXUserForgotPasswordViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *passwordConfirm;

- (void) resetPassword:(UIBarButtonItem *)sender;

@end
