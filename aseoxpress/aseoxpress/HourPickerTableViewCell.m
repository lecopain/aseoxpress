//
//  HourPickerTableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/26/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "HourPickerTableViewCell.h"

@implementation HourPickerTableViewCell

@synthesize hourPickerElement = _hourPickerElement;
@synthesize dataSource = _dataSource;
@synthesize horasData = _horasData;

- (void)awakeFromNib {
    
    
//    NSLog(@"AWAKEN");
    [_hourPickerElement setDelegate:self];
    [_hourPickerElement setDataSource:self];
    
    // Initialization code
    _horasAM = @[@"08:00", @"08:30", @"09:00", @"09:30"];
    _horasPM = @[@"13:00", @"13:30", @"14:00", @"14:30"];
    _horasNone = @[@"Día no disponible"];
    _horasTotal = @[@"08:00", @"08:30", @"09:00", @"09:30", @"13:00", @"13:30", @"14:00", @"14:30"];
    _horasData = (NSMutableArray *)_horasTotal;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (void)setAvailabilityNone{
    [self setHorasData:(NSMutableArray *)_horasNone];
    [self.hourPickerElement reloadAllComponents];
}
- (void)setAvailabilityPM{
    [self setHorasData:(NSMutableArray *)_horasPM];
    [self.hourPickerElement reloadAllComponents];
}
- (void)setAvailabilityAM{
    [self setHorasData:(NSMutableArray *)_horasAM];
    [self.hourPickerElement reloadAllComponents];
}
- (void)setAvailabilityAll{
    [self setHorasData:(NSMutableArray *)_horasTotal];
    [self.hourPickerElement reloadAllComponents];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _horasData.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _horasData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"%@", _horasData[row]);
    if (_dataSource != nil){
        [_dataSource HourAtIndex:_horasData[row]];
    }
}







@end
