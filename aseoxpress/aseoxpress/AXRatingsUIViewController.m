//
//  AXRatingsUIViewController.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 11/19/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXRatingsUIViewController.h"
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"

@interface AXRatingsUIViewController ()

@end

@implementation AXRatingsUIViewController

@synthesize userAlias = _userAlias;
@synthesize idEmpleado = _idEmpleado;
@synthesize empleadoAlias = _empleadoAlias;
@synthesize rating = _rating;
@synthesize commentTextView = _commentTextView;

- (void)viewDidLoad {
    [super viewDidLoad];
    _estrellas = @[@"5 - Excelente", @"4 - Muy bien", @"3 - Bien/Regular", @"2 - No me gustó", @"1 - Pésimo"];
    
    _empleadoAlias.text = _userAlias;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == _estrellasPicker){
        return _estrellas.count;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if(pickerView == _estrellasPicker){
        return _estrellas[row];
    }
    return @"";
}

#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    _rating = 5-row;
    
    if(pickerView == _estrellasPicker){
        
        [self.view endEditing:YES];
        
    }
    
}

-(IBAction)enviarComentarios:(id)sender{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSString * userId = [defaults objectForKey:@"userId"];
    
    NSString *post = [NSString stringWithFormat:@"idcl=%@&idemp=%@&calificacion=%ld&comentarios=%@", userId, _idEmpleado, _rating,  _commentTextView.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.aseoxpress.com/directorvirtual/app/calificarPersonal"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data){
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"aseoxpress.com"
                                                                                                                           message:@"Muchas Gracias! Tu evaluación ha sido enviada."
                                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                                  
                                                  //We add buttons to the alert controller by creating UIAlertActions:
                                                  UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                                                     style:UIAlertActionStyleDefault
                                                                                                   handler:^(UIAlertAction * action){
                                                                                                       [self NavigateToAccount];
                                                                                                   }]; //You can use a block here to handle a press on this button
                                                  [alertController addAction:actionOk];
                                                  [self presentViewController:alertController animated:YES completion:nil];
                                                  
                                                  
                                              
                                              
                                              });
                                          }
                                      }];
    [dataTask resume];
                                      

    
    
    
    
    
}

- (void) NavigateToAccount{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *staffViewController = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"staffScreen"];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[storyboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController: staffViewController];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].view.layer.shouldRasterize = NO;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    [self.navigationController presentViewController:slideNavigationController animated:NO completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
