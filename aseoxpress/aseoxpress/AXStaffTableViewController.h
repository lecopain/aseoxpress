//
//  AXStaffTableViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "PayPalMobile.h"
#import "UIImageView+WebCache.h"

@interface AXStaffTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, SlideNavigationControllerDelegate, PayPalPaymentDelegate>

@property (strong, nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic, strong) NSMutableArray *tableData;

@property (strong, nonatomic) IBOutlet NSArray * jsonObjects;
@property (strong, nonatomic) IBOutlet NSArray * staffImages;
@property (strong, nonatomic) NSString * userId;
@property (strong, nonatomic) NSString * existente;
@property (nonatomic) NSInteger selectedPosition;
@property (strong, nonatomic) NSString * currentPrice;


@property (strong, nonatomic) IBOutlet NSArray * estrellas;
@property (strong , nonatomic) IBOutlet UIPickerView *estrellasPicker;

@property (strong, nonatomic) NSDictionary * serverResponse;

@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;



@end
