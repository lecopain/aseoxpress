//
//  HourPickerTableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/26/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HourPickerDataSource<NSObject>

- (void)HourAtIndex:(NSString *)selectedHour;
@end

@interface HourPickerTableViewCell : UITableViewCell<UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIPickerView* hourPickerElement;

@property (nonatomic, strong) NSArray *horasAM;
@property (nonatomic, strong) NSArray *horasPM;
@property (nonatomic, strong) NSArray *horasNone;
@property (nonatomic, strong) NSArray *horasTotal;
@property (nonatomic, strong) NSMutableArray *horasData;


@property (weak) id <HourPickerDataSource> dataSource;

-(void)setAvailabilityNone;
-(void)setAvailabilityAll;
-(void)setAvailabilityPM;
-(void)setAvailabilityAM;

@end


