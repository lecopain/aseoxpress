//
//  main.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
