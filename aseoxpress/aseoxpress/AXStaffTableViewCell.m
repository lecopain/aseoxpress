//
//  AXStaffTableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 10/21/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "AXStaffTableViewCell.h"

@implementation AXStaffTableViewCell

@synthesize fecha, hora, tipoServicio, notes, referencia, precio, formaPago, estatusPago, nombreEmpleado, profilePic,evaluarStaff, descuento, cargosAdicional, paypalButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
