//
//  UIWebViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/17/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebViewController : UIViewController


@property (nonatomic, strong) IBOutlet UIWebView * webView;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSURL * nsUrl;


@end
