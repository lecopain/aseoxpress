//
//  RatesTableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 4/15/16.
//  Copyright © 2016 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatesTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * rateLabel;


@end
