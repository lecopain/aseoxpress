//
//  TimeCostTableViewCell.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 4/9/16.
//  Copyright © 2016 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol RatePickerDataSource<NSObject>

- (void)RateAtIndex:(NSString *)selectedRate;

@end

@interface RatePickerTableViewCell : UITableViewCell<UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIPickerView* ratePickerElement;

@property (nonatomic, strong) NSArray *rates;
@property (nonatomic, strong) NSMutableArray *ratesMutableArray;
@property (weak) id <RatePickerDataSource> delegate;

//-(void)setRateAvailability;

@end

