//
//  AXRatingsUIViewController.h
//  aseoxpress
//
//  Created by Fernando Jimenez on 11/19/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXRatingsUIViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet NSArray * estrellas;
@property (strong , nonatomic) IBOutlet UIPickerView *estrellasPicker;

@property (strong, nonatomic) NSString * userAlias;
@property (strong, nonatomic) NSString * idEmpleado;

@property (nonatomic) NSInteger rating;
@property (nonatomic, strong) IBOutlet UITextView * commentTextView;

@property (strong, nonatomic) IBOutlet UILabel * empleadoAlias;

-(IBAction)enviarComentarios:(id)sender;


@end
