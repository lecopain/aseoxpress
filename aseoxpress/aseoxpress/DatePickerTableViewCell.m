//
//  DatePickerTableViewCell.m
//  aseoxpress
//
//  Created by Fernando Jimenez on 12/12/15.
//  Copyright © 2015 Fernando Jimenez. All rights reserved.
//

#import "DatePickerTableViewCell.h"

@implementation DatePickerTableViewCell

@synthesize datePickerElement = datePickerElement;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
